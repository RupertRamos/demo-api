//Please remember to add your own MongoDB Atlas connection string and order routes
const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");

mongoose.connect("", {
	useNewUrlParser: true,	
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use("/users", userRoutes);

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});